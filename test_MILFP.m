%--------------------------------------------------------------------------
% file: test for dinkelbach_method
% paper: Optimal Node Selection for Kernel-based Graph Signal 
%           Reconstruction
% authors: M.Coutino, S.P. Chepuri, G. Leus
% conference: ICASSP 2018
%--------------------------------------------------------------------------

N = 1e4;

f = rand(N,1);
g = rand(N,1);

cstFnc = @(A) -sum( f(A) )/sum( g(A) );
opFnc = sfo_fn_wrapper(cstFnc);

Kmax = 10;

p_star = dinklBach_MILFP_card(f,g,Kmax);

if N <= 15
    A_star = sfo_exhaustive_max(opFnc,1:N,Kmax);
    p_exh = zeros(N,1);
    p_exh(A_star) = 1;

    disp('Solutions')
    [p_star p_exh]
end