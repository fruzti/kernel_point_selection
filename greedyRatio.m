%--------------------------------------------------------------------------
% file: greedyratio_method
% paper: Optimal Node Selection for Kernel-based Graph Signal 
%           Reconstruction
% authors: M.Coutino, S.P. Chepuri, G. Leus
% conference: ICASSP 2018
%--------------------------------------------------------------------------

function A = greedyRatio(f,g,V,K)

    A = [];

    for kk = 1:K
        
        h = @(x) -( f([A x]) - f(A) ) / ( g([A x]) - g(A) );
        H = sfo_fn_wrapper(h);
        
        x_star = sfo_greedy_k(H,V,1);
        
        A = [A x_star];
        V = setdiff(V,x_star);
        
    end

end