%--------------------------------------------------------------------------
% file: dinkelbach_method
% paper: Optimal Node Selection for Kernel-based Graph Signal 
%           Reconstruction
% authors: M.Coutino, S.P. Chepuri, G. Leus
% conference: ICASSP 2018
%--------------------------------------------------------------------------

function p_star = dinklBach_MILFP_card(f,g,Kmax,etol)

    itMax = 1e2;
    
    if nargin < 4
        etol = 1e-3;
    end
    
    N = length(f);
    Aeq = ones(1,N);
    lb = zeros(N,1);
    ub = ones(N,1);
    
    x = zeros(N,1);
    ind_0 = randi([1,N],Kmax,1);
    x(ind_0) = 1;
    
    certfkt = inf;
    k = 0;
    while certfkt >= etol && k <=itMax
        k = k + 1;
        w = (f'*x)/(g'*x);
        h = f - w*g;
        [x,fval] = intlinprog(h,N,[],[],Aeq,Kmax,lb,ub);
        certfkt = abs(fval);
        fprintf('Iteration Num: %f\n\n',k);
    end
    p_star = x;
    
end



