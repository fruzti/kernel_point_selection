%==========================================================================
% Report : Optimal Node Selection for Kernel-Based Graph Signal
%                           Reconstruction
% file : monteCarloSombrero.m
%   Test for Comparison of random selection with Optimal Sampling    
%
% Mario Coutino - TU Delft October 2017
%==========================================================================
clear all; close all; clc;
fprintf('-+-+-+-+-+-+-+-+-+-+\n')
fprintf('Example: Sombrero...\n')
fprintf('-+-+-+-+-+-+-+-+-+-+\n')
%% Generate Mesh
rng(0)
fprintf('-Computing Mesh...\n')
Nn = 11;
x = linspace(-10,10,Nn);
xFine = [-10:0.2:10];
[XX,YY] = meshgrid(x);
X = [XX(:) YY(:)];

nNodes = ceil(Nn^2 * 0.8);

Xtr2 = X(randperm(Nn^2,nNodes),:);

bbox = [-10,-10; 10,10];
fd=@(p) drectangle0(p,-10,10,-10,10);

fprintf('-Plotting Mesh...\n')
[p,t]=distmesh2d(fd,@huniform,2,bbox,Xtr2);
close all;
%% Evaluate the sombrero function over every input coordinate.
xx = linspace(-10,10,101);
xx(xx==0) = 0.1;                % avoid division by zero
[XX_o, YY_o] = meshgrid(xx);
X_o = [XX_o(:) YY_o(:)];

hatCenter = [5,-4.5]; hatShift = -0.1;
Z_orig = mexaHat(X_o,hatCenter,hatShift);
%% Plotting mesh w/ surface & grid
fprintf('-Plotting true field...\n')
surf([-10 10],[-10 10],repmat(0, [2 2]),...
reshape(Z_orig, sqrt(size(Z_orig,1)), sqrt(size(Z_orig,1))),'facecolor','texture')
% alpha(0.8)
hold on, trimesh(t,p(:,1),p(:,2),0*ones(size(p(:,1))),'edgecolor','w',...
    'facecolor','none','linewidth',1.5);
view(0,90);
xlim([-10,10])
ylim([-10,10])
plot3(Xtr2(:,1), Xtr2(:,2),0*ones(size(Xtr2,1),1),'wo',...
    'MarkerSize',15)
colormap jet
%% Setting Kernel , Covariance Matrix and Noisy Measurements
% noise covariance
nTr = size(Xtr2,1);
rho = 0.45;
Cn = 0.01*toeplitz(rho.^(0:nTr-1));
Ln = chol(Cn);

% measurements at nodes
% Atr = sqrt(Xtr2(:, 1).^2 + Xtr2(:, 2).^2);
% Ztr = sin(Atr) ./ Atr;
% Ztr(Atr==0) = 1;
Ztr = mexaHat(Xtr2,hatCenter,hatShift);
Ztr_n = Ztr + (Ln' * randn(size(Ztr)));

dstMtx = dist(Xtr2,Xtr2');
sigma = 0.8;
beta = 1 / (2 * sigma^2);
K = exp(-beta * dstMtx);

%measurement covariance
Cv = Cn + Ztr*Ztr';


Ki = pinv(K);
M = Ki*Cv*Ki;
Msqrt = (Ki*sqrtm(Cv))';
%% Test whole solution
fprintf('-Solving full fledge Kernel Problem...\n')
[tred_f,cred_f,Zs_f,~,~,pixError_f] = testSombrero(xFine,Z_orig,Xtr2,Ztr_n,false);
fprintf('\tComp. Time: %f :: Error: %f\n',tred_f,cred_f);
%% Plotting of All Nodes
fprintf('-Plotting Filed Using all nodes...\n')
figure,
surf([-10 10],[-10 10],repmat(0, [2 2]),...
reshape(Zs_f, sqrt(size(Zs_f,1)), sqrt(size(Zs_f,1))),'facecolor','texture')
% alpha(0.8)
hold on, trimesh(t,p(:,1),p(:,2),0*ones(size(p(:,1))),'edgecolor','w',...
    'facecolor','none','linewidth',1.5);
view(0,90);
xlim([-10,10])
ylim([-10,10])
plot3(Xtr2(:,1), Xtr2(:,2),0*ones(size(Xtr2,1),1),'wo',...
    'MarkerSize',15)
plot3(Xtr2(:,1), Xtr2(:,2),0*ones(size(Xtr2,1),1),'r.',...
    'MarkerSize',15)
colormap jet
%% Selection for Sombrero Function
fprintf('-Solving CVX Problem...\n')
k = floor(nTr*0.7);
mu = 0.1;
gamma = mu*k;

mse = @(w) trace( M *...
    (Ki^2 + gamma^-1*Ki*diag(w) + gamma^-1*diag(w)*Ki + gamma^-2*diag(w))^-1);
P = @(w) Ki^2 + gamma^-1*Ki*diag(w) + gamma^-1*diag(w)*Ki + gamma^-2*diag(w);

wsel = solveProbOne(k,Msqrt,P,mse);
%% Test Opt Solution
Xred = Xtr2(wsel>0,:);
Zred = Ztr_n(wsel>0,:);
[tred,cred,Zs,~,~,pixError_red] = testSombrero(xFine,Z_orig,Xred,Zred,false);
fprintf('\tComp. Time: %f :: Error: %f\n',tred,cred);
%% Plotting CVX Solution
fprintf('-Plotting CVX Selection...\n')
figure,
surf([-10 10],[-10 10],repmat(0, [2 2]),...
reshape(Zs, sqrt(size(Zs,1)), sqrt(size(Zs,1))),'facecolor','texture')
hold on, trimesh(t,p(:,1),p(:,2),0*ones(size(p(:,1))),'edgecolor','w',...
    'facecolor','none','linewidth',1.5);
view(0,90);
xlim([-10,10])
ylim([-10,10])
plot3(Xtr2(:,1), Xtr2(:,2),0*ones(size(Xtr2,1),1),'wo',...
    'MarkerSize',15)
plot3(Xred(:,1), Xred(:,2),0*ones(size(Xred,1),1),'k.',...
    'MarkerSize',15)
colormap jet
%% Greedy Selection -not efficient implementation ==rank one update better
fprintf('-Solving Greedy Surrogate...\n')
epsilon = 10;
f = @(xa) logdet(Cv(xa,xa) + epsilon*eye(length(xa)));
g = @(xa) 2*logdet(K(xa,xa) + gamma*eye(length(xa)));

h = @(xa) -f(xa)/g(xa);
H = sfo_fn_wrapper(h);

A_grd = greedyRatio(f,g,1:nTr,k);

q = @(xa) f(xa) - g(xa);
%% Test Greedy Solution
Xred_grd = Xtr2(A_grd(1:k),:);
Zred_grd = Ztr_n(A_grd(1:k),:);
[tred_g,cred_g,Zs_g,~,~,pixError_gred] = testSombrero(xFine,Z_orig,Xred_grd,Zred_grd,false);
fprintf('\tComp. Time: %f :: Error: %f\n',tred_g,cred_g);
%% Plotting Greedy Solution
fprintf('-Plotting Greedy Selection...\n')
figure,
surf([-10 10],[-10 10],repmat(0, [2 2]),...
reshape(Zs_g, sqrt(size(Zs_g,1)), sqrt(size(Zs_g,1))),'facecolor','texture')
% alpha(0.8)
hold on, trimesh(t,p(:,1),p(:,2),0*ones(size(p(:,1))),'edgecolor','w',...
    'facecolor','none','linewidth',1.5);
view(0,90);
xlim([-10,10])
ylim([-10,10])
plot3(Xtr2(:,1), Xtr2(:,2),0*ones(size(Xtr2,1),1),'wo',...
    'MarkerSize',15)
plot3(Xred_grd(:,1), Xred_grd(:,2),0*ones(size(Xred_grd,1),1),'m.',...
    'MarkerSize',15)
colormap jet
%% Plotting Pixel Error
fprintf('-Plotting Pixel Error...\n')
figure,scatter(1:10201,pixError_gred,'.m','linewidth',1.1)
hold on,
scatter(1:10201,pixError_red,'.k','linewidth',1.1)
scatter(1:10201,pixError_f,'.r','linewidth',1.1)
xlim([0,10201])
%% Different Sizes
fprintf('-Solving problem for different sizes\n');

% Greedy
fprintf('\t=Solving Greedy...\n');
A_grd_full = greedyRatio(f,g,1:nTr,nTr);

% CVX
fprintf('\t=Solving CVX Problems...\n');
sizeTests = 20:10:nTr;
Vset = 1:nNodes;
for kk = sizeTests
    
    fprintf('\t\tSolving CVX Size: %d...\n',kk);
    
    wsel = solveProbOne(kk,Msqrt,P,mse);
    
    Xred = Xtr2(wsel>0,:);
    Zred = Ztr_n(wsel>0,:);
    [t_cvx(kk),c_cvx(kk),~,~,~,~] = ...
        testSombrero(xFine,Z_orig,Xred,Zred,false);
    
    Xred_grd = Xtr2(A_grd_full(1:kk),:);
    Zred_grd = Ztr_n(A_grd_full(1:kk),:);
    [t_grd_full(kk), c_grd_f(kk)] = ...
        testSombrero(xFine,Z_orig,Xred_grd,Zred_grd,false);
    mse_cvx(kk) = mse(wsel);
    w_grd = zeros(nTr,1); w_grd(A_grd_full(1:kk)) = 1;
    mse_grd(kk) = mse(w_grd);
    
    det_grd(kk) = q(A_grd_full(1:kk));
    det_cvx(kk) = q(Vset(wsel>0));
end
%% Random Selection
fprintf('-\tRandom Sampling (MC)...\n');
sizeTestsRnd = 20:5:nTr;
nIt = 1e3;
for kk = sizeTestsRnd
    
    fprintf('\t\tRandom Sampling Size: %d...\n',kk);
    for it = 1:nIt
        
        % random sampling of points of fixed cardinality
        indTrainingPnts = randperm(nTr,kk);
        w_rnd = zeros(nTr,1); w_rnd(indTrainingPnts) = 1;
        [~, c_rnd(kk,it)] = ...
        testSombrero(xFine,Z_orig,Xtr2(indTrainingPnts,:),...
            Ztr_n(indTrainingPnts),false);
        
        mse_rnd(kk,it) = mse(w_rnd);
        det_rnd(kk,it) = q(indTrainingPnts);
    end
    
end
%% Plot RegError different sizes
fprintf('-\tPlotting Regression Error...\n');
figure,
h1 = area(sizeTests,max(c_rnd(sizeTests,:),[],2)','LineStyle','none');
hold on,
h2 = area(sizeTests,min(c_rnd(sizeTests,:),[],2)','LineStyle','none');
h1.FaceColor = [0.9 0.9 0.9];
h2.FaceColor = [1 1 1];
p1a = plot(sizeTests,c_grd_f(sizeTests),'-om','linewidth',1.1);
p2a = plot(sizeTests,c_cvx(sizeTests),'-ok','linewidth',1.1);
set(gca,'fontsize',15)
legend([p1a;p2a],{'Greedy Selec.', 'Convex Selec.'})
xlabel('Number of Nodes [K]')
ylabel('Reg. Error')

%% Plot MSE different sizes
fprintf('-\tPlotting MSE Error...\n');
figure,
h1 = area(sizeTests,max(10*log(mse_rnd(sizeTests,:)),[],2)','LineStyle','none');
hold on,
h2 = area(sizeTests,min(10*log(mse_rnd(sizeTests,:)),[],2)','LineStyle','none');
h1.FaceColor = [0.9 0.9 0.9];
h2.FaceColor = [1 1 1];
p1a = plot(sizeTests,10*log(mse_grd(sizeTests)),'-om','linewidth',1.1);
p2a = plot(sizeTests,10*log(mse_cvx(sizeTests)),'-ok','linewidth',1.1);
set(gca,'fontsize',15)
set(gca,'XScale','log');
ylim([8.9 12])
xlim([sizeTests(1) sizeTests(end)])
legend([p1a;p2a],{'Greedy Selec.', 'Convex Selec.'},'location','ne')
xlabel('Number of Nodes [K]')
ylabel('MSE')

%% Plot MSE different sizes
fprintf('-\tPlotting MSE Error...\n');
figure,
h1 = area(sizeTests,max((mse_rnd(sizeTests,:)),[],2)','LineStyle','none');
hold on,
h2 = area(sizeTests,min((mse_rnd(sizeTests,:)),[],2)','LineStyle','none');
h1.FaceColor = [0.9 0.9 0.9];
h2.FaceColor = [1 1 1];
p1a = plot(sizeTests,(mse_grd(sizeTests)),'-om','linewidth',1.1);
p2a = plot(sizeTests,(mse_cvx(sizeTests)),'-ok','linewidth',1.1);
set(gca,'fontsize',15)
set(gca,'XScale','log');
ylim([2 3.5])
xlim([sizeTests(1) sizeTests(end)])
legend([p1a;p2a],{'Greedy Selec.', 'Convex Selec.'},'location','ne')
xlabel('Number of Nodes [K]')
ylabel('MSE')

% save('final_test')

%% Plot logDet different sizes
fprintf('-\tPlotting LogDet Error...\n');
figure,
h1 = area(sizeTests,max((det_rnd(sizeTests,:)),[],2)','LineStyle','none');
hold on,
h2 = area(sizeTests,min((det_rnd(sizeTests,:)),[],2)','LineStyle','none');
h1.FaceColor = 
h2.FaceColor = [1 1 1];
p1a = plot(sizeTests,(det_grd(sizeTests)),'-om','linewidth',1.1);
p2a = plot(sizeTests,(det_cvx(sizeTests)),'-ok','linewidth',1.1);
set(gca,'fontsize',15)
set(gca,'XScale','log');
% ylim([2 3.5])
xlim([sizeTests(1) sizeTests(end)])
legend([p1a;p2a],{'Greedy Selec.', 'Convex Selec.'},'location','ne')
xlabel('Number of Nodes [K]')
ylabel('logdet')