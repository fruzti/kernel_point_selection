Code for paper: "SUBSET SELECTION FOR KERNEL-BASED SIGNAL RECONSTRUCTION"

Conference: ICASSP 2018, Calgary

Autors: M. Coutino(!), S.P. Chepuri(!), G. Leus(!).

Affilations: (!) TU Delft

Contact: m.a.coutinominguez@tudelf.nl

Instructions: To run the code simply execute monteCarloSombrero.m
