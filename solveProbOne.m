
% =========================================================================
% function wsel = solveProbOne(k,Msqrt,P,mse)
%   -> k        : number of nodes to select - scalar
%   -> Msqrt    : M^1/2                     - matrix
%   -> P        : P(w)                      - fnc handler
%   -> mse      : MSE(w)                    - fnc handler
%   <- wsel     : selection vector          - boolean vector
% =========================================================================

function wsel = solveProbOne(k,Msqrt,P,mse)

    N = size(Msqrt,1);
    
    cvx_begin sdp quiet

        variable Z(N,N) symmetric
        variable w(N)
        minimize trace(Z)
        subject to
            ones(N,1)'*w == k;
            0 <= w <= 1;
            sym_cvx([Z       Msqrt';
            Msqrt   P(w)]) >= 0;
     
    cvx_end

    wsel = randomized_rounding(w,k,mse);
    
end
