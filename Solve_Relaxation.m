

function solStruct = Solve_Relaxation(cvxCost, cvxCtrn, numVar, boolSet,...
    eqCtrn)

    
    numCtrn = size(cvxCtrn,1);
    
    cvx_begin quiet

        variable x(numVar)
        minimize ( cvxCost(x) )
        subject to
            for i = 1:numCtrn
                cvxCtrn{i,1}(x) <= cvxCtrn{i,2};
            end
            if nargin > 4
                numEq = size(eqCtrn,1);
                for i = 1:numEq
                    eqCtrn{i,1}(x) == eqCtrn{i,2};
                end
            end
            0 <= x(boolSet) <= 1;

    cvx_end

    solStruct.fval = cvx_optval;
    solStruct.xInt = x;
    solStruct.list = [];

end