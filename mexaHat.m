

function Z_orig = mexaHat(X_o,pDelta, sDelta)

    if nargin < 2
        pDelta = zeros(2,1);
        sDelta = 0;
    elseif nargin < 3
        sDelta = 0;
    end

    A = sqrt((X_o(:, 1)-pDelta(1)).^2 + (X_o(:, 2)-pDelta(2)).^2);
    Z_orig = sin(A-sDelta) ./ (A-sDelta);

end