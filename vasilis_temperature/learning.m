clear
clc
%close all

load('temperatureTimeSeriesData.mat');
A=m_adjacency/max(max(m_adjacency));
Y=m_temperatureTimeSeries;
N=size(A,1);
L=diag(sum(A))-A;
[U,Lambda]=eig(L);
F=40;
F_set=1:F;
Uf=U(:,F_set);

S=50;
sel=[];
        
for cont2=1:S
    
    b=zeros(N,1);
    
    for cont3=1:N
        if sum(find(cont3==sel))==0
            set=[sel cont3];
            d3=zeros(N,1);
            d3(set)=1;
            b(cont3)=abs(det(Uf'*diag(d3)*Uf));
        else
            b(cont3)=-inf;
        end
        
    end
    
    [m,pos]=max(b);
    sel=[sel pos];
    
end

%sel=setdiff(sel,[1]);

sel_vert=sel;
save sel_vert sel_vert

d=zeros(N,1);
d(sel)=1;
D=diag(d);

nosel=setdiff(1:N,sel)


% Reconstruction
Niter=300;
x=25*ones(N,Niter);
mu=1.8;

x2=25*ones(N,Niter);
PSI=zeros(F,F,Niter); PSI(:,:,1)=eye(F);
psi=zeros(F,Niter);
beta=0.05;

for t=1:Niter-1
    
    %d=rand(N,1);
    %D=diag(d<z & d~=0);
    %v=sqrt(Cv)*randn(N,1);
    y=D*Y(:,t);%+D*v;
    
    %LMS
    x(:,t+1)=x(:,t)+mu*Uf*Uf'*D*(y-x(:,t));
    
    %RLS
    PSI(:,:,t+1)=beta*PSI(:,:,t)+Uf'*D*Uf;
    psi(:,t+1)=beta*psi(:,t)+Uf'*D*y;
    x2(:,t+1)=Uf*inv(PSI(:,:,t+1))*psi(:,t+1);
    
end

%p=nosel(1);
p=40;
% figure
% subplot(2,1,1); plot(Y(p,:),'b--')
% subplot(2,1,2);plot(x(p,:),'r')
% xlabel('Time index')
% ylabel('EEG')

figure
plot(Y(p,1:Niter),'b--','linewidth',2)
hold on
plot(x(p,1:Niter),'g','linewidth',2)
hold on
plot(x2(p,1:Niter),'r','linewidth',2)
xlabel('Time index')
ylabel('Temperature estimate')
legend('True signal','LMS','RLS')
grid on
%ylim([30 45])

% hold on
% plot(x2(p,:),'k')

%[xy] = plt_grid(Ad);

