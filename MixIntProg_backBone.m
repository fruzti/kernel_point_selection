%--------------------------------------------------------------------------
% file: mix_integer_programming
% paper: Optimal Node Selection for Kernel-based Graph Signal 
%           Reconstruction
% authors: M.Coutino, S.P. Chepuri, G. Leus
% conference: ICASSP 2018
%--------------------------------------------------------------------------

clear all
% classdef mipTuple
%     properties
%         fValue
%         xReal
%         xInt
%         Cset
%     end
% end

epsilon = 1e-3;

f0 = 3;
qf = [];
iq = {};

%solve cvx
a0 = [2;1;-2];
b0 = [0.7; 0.5;1];
c0 = 1.8;

cvxCos = @(x) norm(x);
cvxCtrn{1,1} = @(x) -b0'*x;  cvxCtrn{1,2} = -c0;
numVar = 3;
boolSet = 1:numVar;

iq{1} = Solve_Relaxation(cvxCos,cvxCtrn,numVar,boolSet);

qf = [qf; iq{1}.fval];
qLen = length(qf);
I = eye(numVar);
%%
while ~isempty(qf)
    
    [~,inx] = min(qf);
    qf(inx) = [];
    % checking integrality
    if norm(round(iq{inx}.xInt) - iq{inx}.xInt) <= epsilon
        
        sol = iq{inx};
        break;
        
    else
        tmpInt = iq{inx}.xInt;
        for i = 1:length(tmpInt)
            if norm(round(tmpInt(i)) - tmpInt(i)) >= epsilon
                nIntIndx = i;
                break;
            end
        end
        tmpList = iq{inx}.list;
        nIntIndx = union(nIntIndx,tmpList);
        iq(inx) = [];
        qLen = length(qf);
        for i = 0:1
        % ------------------
        
        % Equality constraints
        eqCtrn{1,1} = @(x) I(nIntIndx,:)*x; 
        eqCtrn{1,2} = ones(length(nIntIndx),1)*i;
        
        % solve cvx
        tmpStruct = Solve_Relaxation(cvxCos,cvxCtrn,numVar,boolSet,eqCtrn);
        tmpStruct.list = nIntIndx;
        
        % push solution
        iq{qLen+1} = tmpStruct;
        qf = [qf; tmpStruct.fval];

        qLen = length(qf);
        % ------------------
        end
    end
    
end


