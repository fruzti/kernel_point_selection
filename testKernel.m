clear
clc
close all
warning off

load('bcspwr03.mat')

A=full(Problem.A);
A=A-diag(diag(A));
N=size(A,1);

sigma2=0.01;
load Cv Cv
c_v=diag(Cv);

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

% F=10;
% Uf=U(:,1:F);


K = L;
Ki = pinv(K);
M = Ki*Cv*Ki;
Msqrt = (Ki*sqrtm(Cv))';

k = floor(N/2);
mu = 0.1;
gamma = mu*k;

P = @(w) Ki^2 + gamma^-1*Ki*diag(w) + gamma^-1*diag(w)*Ki + gamma^-2*diag(w);
mse = @(w) trace( P(w) * M );

%
f = @(x) log_det(Cv(x,x));
g = @(x) 2*log_det(K(x,x));

h = @(x) -f(x)/g(x);
H = sfo_fn_wrapper(h);

A_grd = greedyRatio(f,g,1:N,N);
val_grd = -1*kSetFunctionCost(H,A_grd');

%%
for k = 50:10:N

    fprintf('Number of Nodes: %d\n\n',k)
    cvx_begin sdp

        variable Z(N,N) symmetric
        variable w(N)
        minimize trace(Z)
        subject to
            ones(N,1)'*w == k;
            0 <= w <= 1;
            sym_cvx([Z       Msqrt';
            Msqrt   P(w)]) >= 0;
     
    cvx_end

    wsel = randomized_rounding(w,k,mse);

    valFnc(k) = mse(wsel);

end

plot(50:10:N,valFnc(50:10:N),'-ob','linewidth',1.1)
hold on,
plot(50:10:N,val_grd(50:10:N),'-xg','linewidth',1.1)



