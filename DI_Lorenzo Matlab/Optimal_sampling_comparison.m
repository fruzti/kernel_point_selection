clear
clc
close all

N=120;
Niter=10;

mu=0.1;
% 
load A A
load x x
load y y
load xy xy
load Cv Cv

% Cv=diag(0.01*rand(N,1));
% 
% %Random Geometric Graph
% px=rand(N,1);
% py=rand(N,1);
% xy=[px py];
% ro=1.2*sqrt(2*log(N)/(pi*N));
% 
% for n=1:N
%     for m=1:N
%         dis = norm([px(n,1),py(n,1)]-[px(m,1),py(m,1)]);
%         if ((dis==0)|(dis>ro))
%             A(n,m)=0;
%         else
%             A(n,m)=1;
%         end
%     end
% end

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

F=3;
Uf=U(:,1:F);

alfa=zeros(1,Niter);
p=zeros(N,Niter);
p2=zeros(N,Niter);

beta=4;
eps=0.08;

% check=1;
% 
% while(check)
%     
%     t=(beta/N)*rand(N,1);
%     
%     if (lambda_min(Uf'*diag(t)*Uf)>=eps) & (ones(N,1)'*t<=beta) & (0<=t<=1)
%         check=0;
%     end
%     
% end

t=(1e-3)*ones(N,1);

p(:,1)=t;
alfa(1)=trace(Uf'*diag(t)*Cv*Uf)/lambda_min(Uf'*diag(t)*Uf);

p2(:,1)=t;
step=1;
gamma=0.001;
tau=1e-6;

MSD=zeros(1,Niter-1);
MSD(1)=trace(inv(Uf'*diag(t)*Uf)*(Uf'*diag(t)*Cv*Uf));

MSD2=zeros(1,Niter-1);
MSD2(1)=trace(inv(Uf'*diag(t)*Uf)*(Uf'*diag(t)*Cv*Uf));

Mask=ones(N,1);


for i=2:Niter
    
    i
    
    % Approximated Problem
    
    cvx_begin quiet
    cvx_solver sedumi
    variable z(N,1)
    minimize (trace(Uf'*diag(z)*Cv*Uf)-alfa(i-1)*lambda_min(Uf'*diag(z)*Uf))
    subject to
    lambda_min(Uf'*diag(z)*Uf)>=eps;
    ones(N,1)'*z<=beta;
    0<=z<=Mask;
    cvx_end
    
    p(:,i)=z;
    alfa(i)=trace(Uf'*diag(z)*Cv*Uf)/lambda_min(Uf'*diag(z)*Uf);
    
    MSD(i)=trace(inv(Uf'*diag(z)*Uf)*(Uf'*diag(z)*Cv*Uf));
    
    % SCA
    
    step=step*(1-gamma*step);
    
    [V,Sigma]=eig(Uf'*diag(p2(:,i-1))*Cv*Uf);
    B12=V*sqrt(inv(Sigma))*V';
    
    cvx_begin quiet
    cvx_solver sedumi
    variable z(N,1)
    minimize (trace_inv(((B12*Uf'*diag(z)*Uf*B12)+(B12*Uf'*diag(z)*Uf*B12)')/2) + trace(inv(Uf'*diag(p2(:,i-1))*Uf)*Uf'*diag(z)*Cv*Uf) + (tau/2)*sum_square(z-p2(:,i-1)) )
    subject to
    lambda_min(Uf'*diag(z)*Uf)>=eps;
    ones(N,1)'*z<=beta;
    0<=z<=Mask;
    cvx_end
    
    p2(:,i)=p2(:,i-1)+step*(z-p2(:,i-1));
    
    MSD2(i)=trace(inv(Uf'*diag(p2(:,i))*Uf)*(Uf'*diag(p2(:,i))*Cv*Uf));
    
end

p(find(p(:,Niter)<=1e-3),Niter)=0;
p2(find(p2(:,Niter)<=1e-3),Niter)=0;


CR=lambda_min(Uf'*diag(p(:,Niter))*Uf)
CR2=lambda_min(Uf'*diag(p2(:,Niter))*Uf)

PB=ones(N,1)'*p(:,Niter)
PB2=ones(N,1)'*p2(:,Niter)

figure
plot(10*log10((mu/2)*MSD),'bo-','linewidth',2)
hold on
plot(10*log10((mu/2)*MSD2),'ro-','linewidth',2)
xlabel('Iteration index')
ylabel('MSD (dB)')
legend('UB','SCA')
grid on

figure
subplot(3,1,1);
bar(Mask,'w')
hold on
bar(p(:,Niter))
xlabel('Node index')
ylabel('Sampling probability')
legend('Mask','UB')
xlim([0 51])
grid on
subplot(3,1,2);
bar(Mask,'w')
hold on
bar(p2(:,Niter),'r')
xlabel('Node index')
ylabel('Sampling probability')
legend('Mask','SCA')
grid on
xlim([0 51])
subplot(3,1,3);
bar(diag(Cv),'g')
xlabel('Node index')
ylabel('Noise variance')
grid on
xlim([0 51])