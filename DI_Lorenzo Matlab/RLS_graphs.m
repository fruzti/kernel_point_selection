clear
clc
close all

N=120;
Niter=200;
Ntrials=200;

mu=1.5;
beta=0.95;

% load A30 A
% load px30 px
% load py30 py
% load Cv Cv

load A A
load x x
load y y
load xy xy
load Cv120 Cv

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

%Signal
F=5;
Uf=U(:,1:F);
Uft=Uf';
s0=zeros(F,1);
s0(1:F)=1;
x0=Uf*s0;

%Optimal Sampling Probabilities
gamma_dB= -25;
gamma=10^(gamma_dB/10);
Cv_inv=inv(Cv);
bound=ones(N,1);

cvx_begin quiet
cvx_solver sedumi
variable p(N,1)
minimize (ones(N,1)'*p)
subject to
trace_inv(Uf'*diag(p)*Cv_inv*Uf)<=gamma*(1+beta)/(1-beta);
zeros(N,1)<=p<=bound;
cvx_end

p(find(abs(p)<=1e-3))=0;

MSD_teo= 10*log10(((1-beta)/(1+beta))*trace_inv(Uf'*diag(p)*Cv_inv*Uf));

figure
subplot(2,1,1);
bar(p)
xlabel('Node index')
ylabel('Sampling probability')
xlim([0 N+1])
grid on
subplot(2,1,2);
bar(diag(Cv),'r')
xlabel('Node index')
ylabel('Noise power')
grid on
xlim([0 N+1])


figure
gplot(A,xy)
colormap(flipud(gray))
hold on
scatter(x,y,70,p,'filled','MarkerEdgeColor','k')

% Algorithm evolution
Cost=zeros(Ntrials,Niter);Cost(:,1)=norm(s0)^2;
Cost2=zeros(Ntrials,Niter);Cost2(:,1)=norm(s0)^2;

for trial=1:Ntrials
    
    trial
    
    %LMS
    s1=zeros(F,Niter);
    
    %RLS
    s2=zeros(F,Niter);
    PSI=zeros(F,F,Niter); PSI(:,:,1)=eye(F);
    psi=zeros(F,Niter);
    
    for t=1:Niter-1
        
        %Observation
        d=rand(N,1);
        D=diag(d<p & d~=0);
        v=sqrt(Cv)*randn(N,1);
        y=D*Uf*s0+D*v;
        
        %LMS
        s1(:,t+1)=s1(:,t)+mu*Uf'*D*(y-Uf*s1(:,t));
        
        %RLS
        PSI(:,:,t+1)=beta*PSI(:,:,t)+Uf'*D*Cv_inv*Uf;
        psi(:,t+1)=beta*psi(:,t)+Uf'*D*Cv_inv*y;
        s2(:,t+1)=inv(PSI(:,:,t+1))*psi(:,t+1);
        
        %MSD
        Cost(trial,t)=norm(s0-s1(:,t))^2;
        Cost2(trial,t)=norm(s0-s2(:,t))^2;
        
    end
    
end

% Plot of the results
if Ntrials>1
    figure
    %line_fewer_markers(1:Niter, 10*log10(mean(Cost)), 10, 'ro','MarkerSize', 8, 'linewidth',1);
    %hold on
    line_fewer_markers(1:Niter, 10*log10(mean(Cost2)), 10, 'rs','MarkerSize', 8, 'linewidth',2);
    hold on
    plot(MSD_teo*ones(Niter,1),'k--','linewidth',2)
    %legend('LMS','RLS','Theory')
    xlabel('iteration index')
    ylabel('MSD (dB)')
    grid on
else
    figure
    line_fewer_markers(1:Niter, 10*log10(Cost), 10, 'ro', 'MarkerSize', 8, 'linewidth',1);
    hold on
    line_fewer_markers(1:Niter, 10*log10(Cost2), 10, 'bs', 'MarkerSize', 8, 'linewidth',1);
    hold on
    plot(MSD_teo*ones(Niter),'g--','linewidth',2)    
    legend('LMS','RLS','Theory')
    xlabel('iteration index')
    ylabel('MSD (dB)')    
    grid on
end
