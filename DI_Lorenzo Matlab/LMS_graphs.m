clear
clc
close all

N=50;
Niter=500;
Ntrials=20;

mu=0.2;
Cv=diag(0.01*rand(N,1));

%Random Geometric Graph
px=rand(N,1);
py=rand(N,1);
xy=[px py];
ro=1.2*sqrt(2*log(N)/(pi*N));
for n=1:N
    for m=1:N
        dis = norm([px(n,1),py(n,1)]-[px(m,1),py(m,1)]);
        if ((dis==0)|(dis>ro))
            A(n,m)=0;
        else
            A(n,m)=1;
        end
    end
end

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

%Signal
F=5;
Uf=U(:,1:F);
s0=zeros(F,1);
s0(1:F)=1;
x0=Uf*s0;

%Sampling
S=5; %number of samples
X=Uf';
sel=[];

for cont2=1:S
    b=zeros(N,1);
    for cont3=1:N
        if sum(find(cont3==sel))==0
            set=[sel cont3];
             b(cont3)=prod(svd(X(:,set)'));
        else
            b(cont3)=-inf;
        end
    end
    [m,pos]=max(b);
    sel=[sel pos];
end

d=zeros(N,1);
d(sel)=1;
D=diag(d);

C1=norm(Uf'*(eye(N)-D)*Uf)

% Algorithm evolution
Cost=zeros(Ntrials,Niter-1);

for trial=1:Ntrials
    
    s=zeros(F,Niter);
    
    for k=1:Niter-1
        v=sqrt(Cv)*randn(N,1);
        y=D*Uf*s0+D*v;
        s(:,k+1)=s(:,k)+mu*Uf'*D*(y-Uf*s(:,k));
        Cost(trial,k)=norm(s0-s(:,k+1))^2;
    end
    
end


% Plot of the results
if Ntrials>1
    figure
    plot(10*log10(mean(Cost)),'r','linewidth',2)
    xlabel('iteration index')
    ylabel('MSD (dB)')
else
    figure
    plot(10*log10(Cost),'r','linewidth',1)
    xlabel('iteration index')
    ylabel('MSD (dB)')    
end

figure
gplot(A,xy)
hold on
scatter(px,py,70,'w','filled','MarkerEdgeColor','k')
hold on
scatter(px(sel),py(sel),70,'k','filled','MarkerEdgeColor','k')
