clear
clc
close all

N=50;
Niter=800;

beta=0.9;
rho=0.1;
c=rho;

load A50 A
load px50 px
load py50 py
xy=[px py];

%Signal
M=10;
R=3;
B=randn(R,M,N);
x0=randn(M,1);
y=zeros(R,N);
for i=1:N
    y(:,i)=B(:,:,i)*x0;
end

% Algorithm evolution
Cost=zeros(1,Niter);
Cost(1)=norm(x0)^2;

x=zeros(M,Niter,N);
Lambda=zeros(M,Niter,N,N);

for t=1:Niter-1
    
    summa=0;
    for i=1:N
        loc_cont=0;
        loc_cont2=0;
        i_link=find(A(:,i)>0);
        for m=1:length(i_link)
            j=i_link(m);
            loc_cont=loc_cont+(Lambda(:,t,i,j)-Lambda(:,t,j,i));
            loc_cont2=loc_cont2+x(:,t,j);
        end
        %x(:,t+1,i)=inv(B(:,:,i)'*B(:,:,i)+rho*sum(A(:,i))*eye(M))*(B(:,:,i)'*y(:,i)-0.5*loc_cont+rho*loc_cont2);
        x(:,t+1,i)=inv(B(:,:,i)'*B(:,:,i)+(((beta)/N)^t)*eye(M))*(B(:,:,i)'*y(:,i)-0.5*loc_cont);
        summa=summa+norm(x0-x(:,t+1,i))^2/N;
    end
    
    Cost(t+1)=summa;
    
    for i=1:N
        for j=1:N
            Lambda(:,t+1,i,j)=Lambda(:,t,i,j)+(c/2)*A(i,j)*(x(:,t+1,i)-x(:,t+1,j));
        end
    end
    
end

figure
semilogy(Cost)
