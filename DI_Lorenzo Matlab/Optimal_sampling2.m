clear
clc
close all

N=120;

mu=0.3;

load A A
load x x
load y y
load xy xy
load Cv120 Cv

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

F=5;
Uf=U(:,1:F);

Niter=15;
alfa=zeros(1,Niter);
p=zeros(N,Niter);

beta_dB=[-32:.5:-20];

MSD=zeros(1,length(beta_dB));
alpha_bar=zeros(1,length(beta_dB));

for k=1:length(beta_dB)
    
    beta=10^(beta_dB(k)/10);
    
    cvx_begin quiet
    %cvx_solver sedumi
    variable z(N,1)
    maximize (lambda_min(Uf'*diag(z)*Uf))
    subject to
    trace(Uf'*diag(z)*Cv*Uf)-(2/mu)*beta*lambda_min(Uf'*diag(z)*Uf)<=0;
    0<=z<=1;
    cvx_end
    
    z(find(z<=1e-3))=0;
    
    %PB=ones(N,1)'*z
    %CR=lambda_min(Uf'*diag(z)*Uf)
    MSD(k)=10*log10((mu/2)*trace(inv(Uf'*diag(z)*Uf)*(Uf'*diag(z)*Cv*Uf)));
    alpha_bar(k)=1-2*mu*lambda_min(Uf'*diag(z)*Uf)
    
end

% figure
% subplot(2,1,1);
% bar(z)
% xlabel('Node index')
% ylabel('Sampling probability')
% xlim([0 N+1])
% grid on
% subplot(2,1,2);
% bar(diag(Cv),'r')
% xlabel('Node index')
% ylabel('Noise power')
% grid on
% xlim([0 N+1])

% figure
% gplot(A,xy)
% hold on
% colormap(flipud(gray))
% scatter(x,y,100,z,'filled','MarkerEdgeColor','k')

figure
plot(beta_dB,alpha_bar,'-bo')



