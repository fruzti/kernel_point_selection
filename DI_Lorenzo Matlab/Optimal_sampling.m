clear
clc
close all

% % N=50;
% % 
% 
 
% % 
% % %Random Geometric Graph
% % px=rand(N,1);
% % py=rand(N,1);
% % xy=[px py];
% % ro=1.2*sqrt(2*log(N)/(pi*N));
% % 
% % for n=1:N
% %     for m=1:N
% %         dis = norm([px(n,1),py(n,1)]-[px(m,1),py(m,1)]);
% %         if ((dis==0)|(dis>ro))
% %             A(n,m)=0;
% %         else
% %             A(n,m)=1;
% %         end
% %     end
% % end
% 

% N1=40;
% N2=40;
% r0=0.28;
% x1=rand(1,N1)-0.7;
% y1=rand(1,N1);
% x2=rand(1,N2)+0.7*cos(pi/3);
% y2=rand(1,N2)+0.7*sin(pi/3);
% x3=rand(1,N2)+0.7*cos(pi/3);
% y3=rand(1,N2)-0.7*sin(pi/3);
% x=[x1,x2,x3];
% y=[y1,y2,y3];
% xy=[x',y'];
% XY=zeros(N,N);
% 
% for k=1:N
%     for l=1:N
%         XY(k,l)=norm(xy(k,:)-xy(l,:));
%     end
% end
% 
% A=XY<r0;

N=120;
load A A
load x x
load y y
load xy xy
%load Cv Cv
load Cv120 Cv
%save Cv120 Cv

%Cv=diag(0.01*rand(N,1));

% load('minnesota.mat')
% A=full(Problem.A);
% xy=Problem.aux.coord;
% x=xy(:,1);
% y=xy(:,2);
% N=size(A,1);
% Cv=diag(ones(N,1));

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

%F_set=[1 2 3 4 5 30 45 60 90 120];
F_set=1:10;
F=length(F_set);

Uf=U(:,F_set);
s0=ones(F,1);
x0=Uf*s0;

Niter=10;
alfa=zeros(1,Niter);
p=zeros(N,Niter);

%Cv=diag(0.01*rand(N,1));

beta=120;
alpha_bar_cons=0.99;
mu=0.1;
eps=(1-alpha_bar_cons)/(2*mu);

% check=1;
% 
% while(check)
% 
%    t=(beta/N)*rand(N,1); 
%     
%    if (lambda_min(Uf'*diag(t)*Uf)>=eps) & (ones(N,1)'*t<=beta) & (0<=t<=1)
%       check=0;
%    end
%    
% end
%
t=0.5*ones(N,1);
p(:,1)=t;
alfa(1)=trace(Uf'*diag(t)*Cv*Uf)/lambda_min(Uf'*diag(t)*Uf);

MSD=zeros(1,Niter-1);
MSD(1)=trace(inv(Uf'*diag(t)*Uf)*(Uf'*diag(t)*Cv*Uf));

%bound=[0.8*ones(N/3,1); 0.3*ones(N/3,1); 0.6*ones(N/3,1)];
bound=ones(N,1);

for i=2:Niter
    
    i
    
    cvx_begin quiet
    cvx_solver sedumi
    variable z(N,1)
    minimize (trace(Uf'*diag(z)*Cv*Uf)-alfa(i-1)*lambda_min(Uf'*diag(z)*Uf))
        subject to
        lambda_min(Uf'*diag(z)*Uf)>=eps;
        ones(N,1)'*z<=beta;
        zeros(N,1)<=z<=bound;
    cvx_end
    
    p(:,i)=z;
    alfa(i)=trace(Uf'*diag(z)*Cv*Uf)/lambda_min(Uf'*diag(z)*Uf);
    
    MSD(i)=trace(inv(Uf'*diag(z)*Uf)*(Uf'*diag(z)*Cv*Uf));
    
end

p(find(p(:,Niter)<=1e-3),Niter)=0;

alpha_bar=1-2*mu*lambda_min(Uf'*diag(p(:,Niter))*Uf)

figure
% plot(10*log10((mu/2)*lambda),'bo-')
% hold on
plot(10*log10((mu/2)*MSD),'b--','linewidth',2)
xlabel('Iteration index')
ylabel('MSD (dB)')
grid on

figure
subplot(2,1,1);
bar(p(:,Niter))
xlabel('Node index')
ylabel('Sampling probability')
hold on
plot(bound,'--g','linewidth',2)
xlim([0 N+1])
ylim([0 1])
grid on
subplot(2,1,2);
bar(diag(Cv),'r')
xlabel('Node index')
ylabel('Noise power')
grid on
xlim([0 N+1])

figure
gplot(A,xy)
hold on
colormap(flipud(gray))
scatter(x,y,57,p(:,Niter),'filled','MarkerEdgeColor','k')

p=p(:,Niter);

break

Ntrials=300;
Niter2=2000;

% Algorithm evolution
Cost=zeros(Ntrials,Niter2);Cost(:,1)=norm(s0)^2;

for trial=1:Ntrials
    
    trial
    
    %LMS
    s1=zeros(F,Niter2);
    
    %RLS
    s2=zeros(F,Niter2);
    PSI=zeros(F,F,Niter2); PSI(:,:,1)=eye(F);
    psi=zeros(F,Niter2);
    
    for t=1:Niter2-1
        
        %Observation
        d=rand(N,1);
        D=diag(d<p & d~=0);
        v=sqrt(Cv)*randn(N,1);
        y=D*Uf*s0+D*v;
        
        %LMS
        s1(:,t+1)=s1(:,t)+mu*Uf'*D*(y-Uf*s1(:,t));
                
        %MSD
        Cost(trial,t)=norm(s0-s1(:,t))^2;
        
    end
    
end

% Plot of the results
if Ntrials>1
    figure
    line_fewer_markers(1:Niter2, 10*log10(mean(Cost)), 10, 'bo','MarkerSize', 10, 'linewidth',2);
    xlabel('iteration index')
    ylabel('MSD (dB)')
    hold on
    plot(10*log10((mu/2)*MSD(Niter))*ones(1,Niter2),'g--','linewidth',2)
    grid on
else
    figure
    line_fewer_markers(1:Niter, 10*log10(Cost), 10, 'bo', 'MarkerSize', 10, 'linewidth',2);
    xlabel('iteration index')
    ylabel('MSD (dB)')    
    grid on
end


%h = legend('$\bar{\alpha}=0.95$', '$\bar{\alpha}=0.97$', '$\bar{\alpha}=0.98$', 'Theoretical MSD');
%set(h,'Interpreter','latex')








