clear
clc
close all

N=30;
Niter=1200;
Ntrials=50;

mu=0.4;
beta=0.95;
rho=9;
K=4;

load A30 A
load px30 px
load py30 py
load Cv Cv
%Cv=eye(N);
xy=[px py];

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

%Signal
F=5;
Uf=U(:,1:F);
Uft=Uf';
s0=zeros(F,1);
s0(1:F)=1;
x0=Uf*s0;

% %Optimal Sampling Probabilities
gamma_dB= -20;
% gamma=10^(gamma_dB/10);
% Cv_inv=inv(Cv);
% bound=ones(N,1);
% 
% bound=ones(N,1);
% cvx_begin 
% cvx_solver sedumi
% variable p(N,1)
% minimize (ones(N,1)'*p)
% subject to
% trace_inv(Uf'*diag(p)*Cv_inv*Uf)<=gamma*(1+beta)/(1-beta);
% zeros(N,1)<=p<=bound;
% cvx_end
% 
% p(find(abs(p)<=1e-3))=0;
% 
% MSD_teo= 10*log10(((1-beta)/(1+beta))*trace_inv(Uf'*diag(p)*Cv_inv*Uf));

load p p

% % Algorithm evolution
% Cost=zeros(Ntrials,Niter);Cost(:,1)=norm(s0)^2;
% Cost2=zeros(Ntrials,Niter);Cost2(:,1)=norm(s0)^2;
Cost3=zeros(Ntrials,Niter);Cost3(:,1)=norm(s0)^2;

for trial=1:Ntrials
    
    trial
    
%     %LMS
%     s1=zeros(F,Niter);
%     
%     %RLS
%     s2=zeros(F,Niter);
%     PSI=zeros(F,F,Niter); PSI(:,:,1)=eye(F);
%     psi=zeros(F,Niter);
%     
    %Distributed RLS
    P=zeros(F,F,Niter,N);
    delta=1/N;
    for i=1:N
       P(:,:,1,i)=eye(F)*delta;
    end
    Dpsi=zeros(F,Niter,N);
    Lambda=zeros(F,Niter,K,N,N);
    s3=zeros(F,Niter,K,N);
       
    for t=1:Niter-1
        
        %Observation
        d=rand(N,1);
        D=diag(d<p & d~=0);
        v=sqrt(Cv)*randn(N,1);
        y=D*Uf*s0+D*v;
        
%         %LMS
%         s1(:,t+1)=s1(:,t)+mu*Uf'*D*(y-Uf*s1(:,t));
%         
%         %RLS
%         PSI(:,:,t+1)=beta*PSI(:,:,t)+Uf'*D*Cv_inv*Uf;
%         psi(:,t+1)=beta*psi(:,t)+Uf'*D*Cv_inv*y;
%         s2(:,t+1)=inv(PSI(:,:,t+1))*psi(:,t+1);
        
        %Distributed RLS
        G=zeros(F,F);
        for i=1:N
            d_tilde=D(i,i)/Cv(i,i);
            Dpsi(:,t+1,i)=beta*Dpsi(:,t,i)+d_tilde*y(i)*Uft(:,i);
            P(:,:,t+1,i)=beta*P(:,:,t,i)+d_tilde*Uft(:,i)*Uft(:,i)';
        end
        
        for k=1:K-1
            summa=0;
            for i=1:N
                loc_cont=0;
                i_link=find(A(:,i)>0);
                for m=1:length(i_link)
                    j=i_link(m);
                    loc_cont=loc_cont+(Lambda(:,t,k,i,j)-Lambda(:,t,k,j,i));
                    %loc_cont2=loc_cont2+s3(:,t,k,j);
                end
                loc_cont2=sum(s3(:,t,k,i_link),4);
                s3(:,t,k+1,i)=inv(P(:,:,t+1,i)+rho*sum(A(:,i))*eye(F))*(Dpsi(:,t+1,i)-0.5*loc_cont+rho*loc_cont2);
                if k==K-1
                   summa=summa+norm(s0-s3(:,t,k+1,i))^2/N;
                   s3(:,t+1,1,i)=s3(:,t,k+1,i);
                end
            end
            for i=1:N
                for j=1:N
                    Lambda(:,t,k+1,i,j)=Lambda(:,t,k,i,j)+(rho/2)*A(i,j)*(s3(:,t,k+1,i)-s3(:,t,k+1,j));
                    if k==K-1
                        Lambda(:,t+1,1,i,j)=Lambda(:,t,k+1,i,j);
                    end
                end
            end
        end
       
        %MSD
        %Cost(trial,t+1)=norm(s0-s1(:,t))^2;
        %Cost2(trial,t+1)=norm(s0-s2(:,t))^2;
        Cost3(trial,t+1)=summa;
    
    end
    
end

% Plot of the results
if Ntrials>1
    figure
%     line_fewer_markers(1:Niter, 10*log10(mean(Cost)), 10, 'ro','MarkerSize', 8, 'linewidth',1);
%     hold on
%     line_fewer_markers(1:Niter, 10*log10(mean(Cost2)), 10, 'bs','MarkerSize', 8, 'linewidth',1);
%     hold on
    line_fewer_markers(1:Niter, 10*log10(mean(Cost3)), 10, 'gp','MarkerSize', 8, 'linewidth',1);
%     hold on
%     plot(MSD_teo*ones(Niter,1),'g--','linewidth',2)
%    legend('LMS','RLS','Distributed RLS','Theory')
    xlabel('iteration index')
    ylabel('MSD (dB)')
    grid on
else
    figure
%     line_fewer_markers(1:Niter, 10*log10(Cost), 10, 'ro', 'MarkerSize', 8, 'linewidth',1);
%     hold on
%     line_fewer_markers(1:Niter, 10*log10(Cost2), 10, 'bs', 'MarkerSize', 8, 'linewidth',1);
%     hold on
    line_fewer_markers(1:Niter, 10*log10(Cost3), 10, 'kd','MarkerSize', 8, 'linewidth',1);
    hold on
    plot(gamma_dB*ones(Niter),'g--','linewidth',2)    
%    legend('LMS','RLS','Distributed RLS','Theory')
    xlabel('iteration index')
    ylabel('MSD (dB)')    
    grid on
end
