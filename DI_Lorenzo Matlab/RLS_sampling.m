clear
clc
close all

N=120;

beta=0.9;

load A A
load x x
load y y
load xy xy
load Cv120 Cv

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

%Signal
F=5;
Uf=U(:,1:F);
Uft=Uf';
s0=zeros(F,1);
s0(1:F)=1;
x0=Uf*s0;

%Optimal Sampling Probabilities
gamma_dB= -35:.1:-10;

rate=zeros(1,length(gamma_dB));

for k=1:length(gamma_dB)
    
    gamma=10^(gamma_dB(k)/10);
    Cv_inv=inv(Cv);
    bound=ones(N,1);
    
    cvx_begin quiet
    cvx_solver sedumi
    variable p(N,1)
    minimize (ones(N,1)'*p)
    subject to
    trace_inv(Uf'*diag(p)*Cv_inv*Uf)<=gamma*(1+beta)/(1-beta);
    zeros(N,1)<=p<=bound;
    cvx_end
    
    p(find(abs(p)<=1e-3))=0;
    
    rate(k)=ones(N,1)'*p;
    
end

%MSD_teo= 10*log10(((1-beta)/(1+beta))*trace_inv(Uf'*diag(p)*Cv_inv*Uf));

figure
line_fewer_markers(gamma_dB, rate, 10, 'kp','MarkerSize', 8, 'linewidth',2);
