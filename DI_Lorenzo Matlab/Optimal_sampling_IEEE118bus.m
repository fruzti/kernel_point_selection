clear
clc
close all
warning off

load('bcspwr03.mat')

A=full(Problem.A);
A=A-diag(diag(A));
N=size(A,1);

sigma2=0.01;
load Cv Cv
c_v=diag(Cv);

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

F=10;
Uf=U(:,1:F);

%%
% Sampling strategy 1
gamma_dB=-25;
gamma=10^(gamma_dB/10);
mu=0.1;
bound=ones(N,1);
alpha_set=0.9:0.01:0.99;

Ntrials=100;

SR=zeros(1,length(alpha_set));
SR2=zeros(1,length(alpha_set));
SR3=zeros(Ntrials,length(alpha_set));
SR4=zeros(Ntrials,length(alpha_set));


for k=1:length(alpha_set)
    
    k
    
    alpha=alpha_set(k);
    eps=(1-alpha)/(2*mu);
    
    % Strategy 1
    
    cvx_begin
    %cvx_solver sedumi
    variable z(N,1)
    minimize (sum(z))
    subject to
    trace(Uf'*diag(z)*Cv*Uf)-(2/mu)*gamma*lambda_min(Uf'*diag(z)*Uf)<=0;
    lambda_min(Uf'*diag(z)*Uf)>=eps;
    0<=z<=bound;
    cvx_end
    
    SR(k)=sum(z);
    
    
    % Max det strategy
    sel=[];
    z=zeros(N,1);
    
    for cont2=1:N
        
        b=zeros(N,1);
        
        for cont3=1:N
            if sum(find(cont3==sel))==0
                set=[sel cont3];
                d3=zeros(N,1);
                d3(set)=1;
                %b(cont3)=abs(det(Uf'*diag(d3./c_v)*Uf));
                b(cont3)=abs(det(Uf'*diag(d3)*Uf));
            else
                b(cont3)=-inf;
            end
            
        end
        
        [m,pos]=max(b);
        sel=[sel pos];
        z(sel)=1;
        
        MSD=(mu/2)*trace(inv(Uf'*diag(z)*Uf)*(Uf'*diag(z)*Cv*Uf));
        rate=lambda_min(Uf'*diag(z)*Uf);
        
        if (cont2>=F && MSD<=gamma && rate>=eps) || cont2==N
            SR2(k)=sum(z);
            break;
        end
        
    end
    
    
    for t=1:Ntrials
    
%    Random sampling

        sel=[];
        Set=1:N;
        z=zeros(N,1);
        
        for cont2=1:N
            
            Set2=setdiff(Set,sel);
            c2=randsample(Set2,1);
            sel=[sel c2];
            z(c2)=1;
            
            MSD=(mu/2)*trace(inv(Uf'*diag(z)*Uf)*(Uf'*diag(z)*Cv*Uf));
            rate=lambda_min(Uf'*diag(z)*Uf);
            
            if (cont2>=F && MSD<=gamma && rate>=eps) || cont2==N
                SR3(t,k)=sum(z);
                break;
            end
            
        end
        
%    Leverage score sampling

        sel=[];
        Set=1:N;
        z=zeros(N,1);
        
        for cont2=1:N
            
            Set2=setdiff(Set,sel);
            X=Uf(Set2,:)';
            weights=diag(sqrt(X'*X));
            c2=randsample(Set2,1,true,weights);
            sel=[sel c2];
            z(c2)=1;
            
            MSD=(mu/2)*trace(inv(Uf'*diag(z)*Uf)*(Uf'*diag(z)*Cv*Uf));
            rate=lambda_min(Uf'*diag(z)*Uf);
            
            if (cont2>=F && MSD<=gamma && rate>=eps) || cont2==N
                SR4(t,k)=sum(z);
                break;
            end
            
        end                
        
    end
    
end

figure
plot(alpha_set,SR,'o-','linewidth',2)
hold on
plot(alpha_set,SR2,'rs-','linewidth',2)
hold on
plot(alpha_set,mean(SR3),'kd-','linewidth',2)
hold on
plot(alpha_set,mean(SR4),'gx-','linewidth',2)
xlabel('$\bar{\alpha}$','interpreter','latex')
ylabel('Graph sampling rate')
grid on