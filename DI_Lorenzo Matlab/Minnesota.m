clear
clc
close all

load('minnesota.mat')
A=full(Problem.A);
Coord=Problem.aux.coord;
N=size(A,1);

% figure
% gplot(A,Coord),hold on,
% scatter(Coord(:,1),Coord(:,2),30,'w','filled','MarkerEdgeColor','k');

L=diag(sum(A'))-A;
[U,V]=eig(L);

c=3;
s=exp(-c*diag(V));
%s=s/norm(s);
%F=50;
%s=[1:F zeros(1,N-F)]';

figure
%plot(diag(V),s)
plot(s)

x=U*s;

figure
gplot(A,Coord),hold on,
scatter(Coord(:,1),Coord(:,2),22,x,'filled','MarkerEdgeColor','k');