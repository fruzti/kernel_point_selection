clear
clc
close all

N=120;
Niter=40;
Ntrials=1;

%Cv=diag(0.01*rand(N,1));

load A A
load x x
load y y
load xy xy
load Cv120 Cv

% %Random Geometric Graph
% px=rand(N,1);
% py=rand(N,1);
% xy=[px py];
% ro=1.2*sqrt(2*log(N)/(pi*N));
% 
% for n=1:N
%     for m=1:N
%         dis = norm([px(n,1),py(n,1)]-[px(m,1),py(m,1)]);
%         if ((dis==0)|(dis>ro))
%             A(n,m)=0;
%         else
%             A(n,m)=1;
%         end
%     end
% end

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

F=5;
Uf=U(:,1:F);

alpha_bar_cons=0.9
mu=0.1;
eps=(1-alpha_bar_cons)/(2*mu);


for trial=1:Ntrials
    
    trial
    
%     p=zeros(N,Niter);
%     
%     check=1;
%     
%     while(check)
%         
%         t=(beta/N)*rand(N,1);
%         
%         if (lambda_min(Uf'*diag(t)*Uf)>=eps) & (ones(N,1)'*t<=beta) & (0<=t<=1)
%             check=0;
%         end
%         
%     end

    t=0.5*ones(N,1);
    p(:,1)=t;
    
    MSD=zeros(1,Niter);
    MSD(1)=trace(inv(Uf'*diag(t)*Uf)*(Uf'*diag(t)*Cv*Uf));
    
    alpha=1;
    gamma=0.001;
    tau=1e-6;
    
    
    for i=2:Niter
        
        i
        
        alpha=alpha*(1-gamma*alpha);
        
        [V,Sigma]=eig(Uf'*diag(p(:,i-1))*Cv*Uf);
        B12=V*sqrt(inv(Sigma))*V';
        
        cvx_begin quiet
        cvx_solver sedumi
        variable z(N,1)
        minimize (trace_inv(((B12*Uf'*diag(z)*Uf*B12)+(B12*Uf'*diag(z)*Uf*B12)')/2) + trace(inv(Uf'*diag(p(:,i-1))*Uf)*Uf'*diag(z)*Cv*Uf) + (tau/2)*sum_square(z-p(:,i-1)) )
        subject to
        lambda_min(Uf'*diag(z)*Uf)>=eps;
        0<=z<=1;
        cvx_end
        
        p(:,i)=p(:,i-1)+alpha*(z-p(:,i-1));
        
        MSD(i)=trace(inv(Uf'*diag(p(:,i))*Uf)*(Uf'*diag(p(:,i))*Cv*Uf));
        
    end
    
    p(find(p(:,Niter)<=1e-3),Niter)=0;
    
    %eps2=mu*lambda_min(Uf'*diag(p(:,Niter))*Uf)
    alpha_bar=1-2*mu*lambda_min(Uf'*diag(p(:,Niter))*Uf)
    
    if Ntrials==1
        
        figure
        % plot(10*log10((mu/2)*lambda),'bo-')
        % hold on
        %plot(10*log10((mu/2)*MSD),'ro-','linewidth',2)
        hold on
        line_fewer_markers(1:Niter, 10*log10((mu/2)*MSD), 10, 'bo', 'MarkerSize', 10, 'linewidth',2);
        xlabel('Iteration index')
        ylabel('MSD (dB)')
        grid on
        
        figure
        subplot(2,1,1);
        bar(p(:,Niter))
        xlabel('Node index')
        ylabel('Sampling probability')
        xlim([0 N+1])
        grid on
        subplot(2,1,2);
        bar(diag(Cv),'r')
        xlabel('Node index')
        ylabel('Noise power')
        grid on
        xlim([0 N+1])
        
        figure
        gplot(A,xy)
        hold on
        colormap(flipud(gray))
        scatter(x,y,50,p(:,Niter),'filled','MarkerEdgeColor','k')
        
    else
        
        figure(1)
        plot(10*log10((mu/2)*MSD),'ro-','linewidth',2)
        xlabel('Iteration index')
        ylabel('MSD (dB)')
        grid on
        hold on
        
    end

end


