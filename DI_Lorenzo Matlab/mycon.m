function [c,ceq] = mycon(x,Uf,alpha,mu)
c=lambda_min(Uf'*diag(x)*Uf)-((1-alpha)/(2*mu));
ceq=[];