clear
clc
close all

N=50;
Niter=500;
Ntrials=1;

mu=0.2;
beta=0.95;
rho=100;

load A50 A
load px50 px
load py50 py
load Cv Cv
%Cv=eye(N);
xy=[px py];

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

%Signal
F=5;
Uf=U(:,1:F);
Uft=Uf';
s0=zeros(F,1);
s0(1:F)=1;
x0=Uf*s0;

%Optimal Sampling Probabilities
gamma_dB= -30;
gamma=10^(gamma_dB/10);
Cv_inv=inv(Cv);
bound=ones(N,1);

bound=ones(N,1);
cvx_begin 
cvx_solver sedumi
variable p(N,1)
minimize (ones(N,1)'*p)
subject to
trace_inv(Uf'*diag(p)*Cv_inv*Uf)<=gamma*(1+beta)/(1-beta);
zeros(N,1)<=p<=bound;
cvx_end

p(find(abs(p)<=1e-3))=0;

MSD_teo= 10*log10(((1-beta)/(1+beta))*trace_inv(Uf'*diag(p)*Cv_inv*Uf));

% Algorithm evolution
Cost=zeros(Ntrials,Niter);Cost(:,1)=norm(s0)^2;
Cost2=zeros(Ntrials,Niter);Cost2(:,1)=norm(s0)^2;
Cost3=zeros(Ntrials,Niter);Cost3(:,1)=norm(s0)^2;

for trial=1:Ntrials
    
    trial
    
    %LMS
    s1=zeros(F,Niter);
    
    %RLS
    s2=zeros(F,Niter);
    PSI=zeros(F,F,Niter); PSI(:,:,1)=eye(F);
    psi=zeros(F,Niter);
    
    %Distributed RLS
    P=zeros(F,F,Niter,N);
    delta=1/N;
    for i=1:N
       P(:,:,1,i)=eye(F)*delta;
    end
    Dpsi=zeros(F,Niter,N);
    Lambda=zeros(F,Niter,N,N);
    s3=zeros(F,Niter,N);
       
    for t=2:Niter
        
        %Observation
        d=rand(N,1);
        D=diag(d<p & d~=0);
        v=sqrt(Cv)*randn(N,1);
        y=D*Uf*s0+D*v;
        
        %LMS
        s1(:,t)=s1(:,t-1)+mu*Uf'*D*(y-Uf*s1(:,t-1));
        
        %RLS
        PSI(:,:,t)=beta*PSI(:,:,t-1)+Uf'*D*Cv_inv*Uf;
        psi(:,t)=beta*psi(:,t-1)+Uf'*D*Cv_inv*y;
        s2(:,t)=inv(PSI(:,:,t))*psi(:,t);
        
        %Distributed RLS
        G=zeros(F,F);
        summa=0;
        for i=1:N
            d_tilde=D(i,i)/Cv(i,i);
            Dpsi(:,t,i)=beta*Dpsi(:,t-1,i)+d_tilde*y(i)*Uft(:,i);
            P(:,:,t,i)=beta*P(:,:,t-1,i)+d_tilde*Uft(:,i)*Uft(:,i)';
            loc_cont=0;
            loc_cont2=0;
            i_link=find(A(:,i)>0);
            for m=1:length(i_link)
                j=i_link(m);
                loc_cont=loc_cont+(Lambda(:,t-1,i,j)-Lambda(:,t-1,j,i));
                loc_cont2=loc_cont2+s3(:,t-1,j);
            end
            s3(:,t,i)=inv(P(:,:,t,i)+rho*sum(A(:,i))*eye(F))*(Dpsi(:,t,i)-0.5*loc_cont+rho*loc_cont2);
            summa=summa+norm(s0-s3(:,t,i))^2/N;
        end
        for i=1:N
            for j=1:N
                Lambda(:,t,i,j)=Lambda(:,t-1,i,j)+(rho/2)*A(i,j)*(s3(:,t,i)-s3(:,t,j));
            end
        end
       
        %MSD
        Cost(trial,t)=norm(s0-s1(:,t))^2;
        Cost2(trial,t)=norm(s0-s2(:,t))^2;
        Cost3(trial,t)=summa;
    
    end
    
end

% Plot of the results
if Ntrials>1
    figure
    line_fewer_markers(1:Niter, 10*log10(mean(Cost)), 10, 'ro','MarkerSize', 8, 'linewidth',1);
    hold on
    line_fewer_markers(1:Niter, 10*log10(mean(Cost2)), 10, 'bs','MarkerSize', 8, 'linewidth',1);
    hold on
    line_fewer_markers(1:Niter, 10*log10(mean(Cost3)), 10, 'kd','MarkerSize', 8, 'linewidth',1);
    hold on
    plot(MSD_teo*ones(Niter,1),'g--','linewidth',2)
    legend('LMS','RLS','Distributed RLS','Theory')
    xlabel('iteration index')
    ylabel('MSD (dB)')
    grid on
else
    figure
    line_fewer_markers(1:Niter, 10*log10(Cost), 10, 'ro', 'MarkerSize', 8, 'linewidth',1);
    hold on
    line_fewer_markers(1:Niter, 10*log10(Cost2), 10, 'bs', 'MarkerSize', 8, 'linewidth',1);
    hold on
    line_fewer_markers(1:Niter, 10*log10(Cost3), 10, 'kd','MarkerSize', 8, 'linewidth',1);
    hold on
    plot(MSD_teo*ones(Niter),'g--','linewidth',2)    
    legend('LMS','RLS','Distributed RLS','Theory')
    xlabel('iteration index')
    ylabel('MSD (dB)')    
    grid on
end
