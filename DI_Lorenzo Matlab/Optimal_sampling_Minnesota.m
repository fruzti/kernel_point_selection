clear
clc
close all

% gamma_dB=-20;
% gamma=10^(gamma_dB/10);
% beta=0.95;

load('minnesota.mat')
A=full(Problem.A);
xy=Problem.aux.coord;
x=xy(:,1);
y=xy(:,2);
N=size(A,1);

sigma2=0.01;
Cv=sigma2*diag(rand(N,1));
Cv_inv=(1/sigma2)*diag(ones(N,1));

L=diag(sum(A'))-A;
[U,Lambda]=eig(L);

F=10;
Uf=U(:,1:F);

beta_dB=-34;
beta=10^(beta_dB/10);
eps=0.05;
mu=0.1;

cvx_begin
%cvx_solver sedumi
cvx_precision low
variable z(N,1)
minimize (sum(z))
subject to
trace(Uf'*diag(z)*Cv*Uf)-(2/mu)*beta*lambda_min(Uf'*diag(z)*Uf)<=0;
lambda_min(Uf'*diag(z)*Uf)>=eps;
%trace_inv(Uf'*diag(z)*Cv_inv*Uf)<=gamma*(1+beta)/(1-beta);
0<=z<=1;
cvx_end

% cvx_begin
% %cvx_precision low
% cvx_solver sedumi
% variable z(N,1)
% minimize (sum(z))
% subject to
% trace(Uf'*diag(z)*Cv*Uf)-(2/mu)*gamma*lambda_min(Uf'*diag(z)*Uf)<=0;
% %lambda_min(Uf'*diag(z)*Uf)>=eps;
% 0<=z<=1;
% cvx_end

% cvx_begin
% cvx_precision low
% %cvx_solver sedumi
% variable z(N,1)
% maximize (lambda_min(Uf'*diag(z)*Uf))
% subject to
% trace(Uf'*diag(z)*Cv*Uf)-(2/mu)*gamma*lambda_min(Uf'*diag(z)*Uf)<=0;
% %lambda_min(Uf'*diag(z)*Uf)>=eps;
% 0<=z<=1;
% cvx_end

% 
% alpha=0.99;
% mu=0.1;
% x0=0.5*ones(N,1);
% %options = optimset('Algorithm','interior-point'); % run interior-point algorithm
% [z,FVAL,EXITFLAG] = fmincon(@(x) myfun(x,Uf,Cv),x0,[],[],[],[],zeros(N,1),ones(N,1),@(x) mycon(x,Uf,alpha,mu))

% eta=1400;
% cvx_begin
% %cvx_solver sedumi
% variable z(N,1)
% minimize (trace_inv(Uf'*diag(z)*Cv_inv*Uf))
% subject to
% %trace(Uf'*diag(z)*Cv*Uf)-(2/mu)*beta*lambda_min(Uf'*diag(z)*Uf)<=0;
% %lambda_min(Uf'*diag(z)*Uf)>=eps;
% sum(z)<=eta;
% 0<=z<=1;
% cvx_end

%MSD=10*log10((1-beta)/(1+beta)*trace_inv(Uf'*diag(z)*Cv_inv*Uf))

MSD=10*log10((mu/2)*trace(inv(Uf'*diag(z)*Uf)*Uf'*diag(z)*Cv*Uf))

%z(find(z<=1e-3))=0;

figure
subplot(2,1,1);
bar(z)
xlabel('Node index')
ylabel('Sampling probability')
xlim([0 N+1])
grid on
subplot(2,1,2);
bar(diag(Cv),'r')
xlabel('Node index')
ylabel('Noise power')
grid on
xlim([0 N+1])

figure
gplot(A,xy)
hold on
colormap(flipud(gray))
scatter(x,y,30,z,'filled','MarkerEdgeColor','k')
