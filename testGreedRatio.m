
M = 10;
V = 1:M;

Arnd = randn(M);
C = Arnd*Arnd';

Brnd = randn(M);
K = Brnd*Brnd';

f = @(x) log_det(C(x,x));
g = @(x) 2*log_det(K(x,x));


h = @(x) -f(x)/g(x);
H = sfo_fn_wrapper(h);

A_grd = greedyRatio(f,g,V,M);
val_grd = -1*kSetFunctionCost(H,A_grd');

val_exh = zeros(M,1);
A_exh = cell(M);

for mm = 1:M
    A_exh{mm} = sfo_exhaustive_max(H,V,mm);
    val_exh(mm) = -1*h(A_exh{mm});
end

p1 = plot(V,val_grd,'-g','linewidth',1.1);
hold on, 
p2 = plot(V,val_exh,'-b','linewidth',1.1);

legend([p1;p2],{'Greedy','Exhaustive'})